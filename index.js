console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

let firstName = "Oliver"
console.log("First Name: " + firstName)
let lastName = "Leonor"
console.log("Last Name: " + lastName)
let age = 30
console.log("Age: " + age)
let hobbies = ["hiking", "swimming", "playing"];
console.log(hobbies)
let address = {
	houseNumber: "13",
	street: "Saarland",
	city: "Antipolo",
	country: "Philippines",
}
console.log(address)


	let fullName = "Oliver Leonor ";
	console.log("My full name is " + fullName);

	let currentAge = 32;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Amber","Donkey","Muning","Doggie","White","Goku"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_philippines",
		fullName: "Oliver Leonor",
		age: 32,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName1 = "Ruru Torres";
	console.log("My bestfriend is: " + fullName1);

	const lastLocation = "Antipolo City";
	
	console.log("I was found frozen in: " + lastLocation);

